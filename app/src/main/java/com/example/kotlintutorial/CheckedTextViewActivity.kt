package com.example.kotlintutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CheckedTextView
import android.widget.Toast

class CheckedTextViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checked_text_view)
        val ctView = findViewById<CheckedTextView>(R.id.ctv)
        ctView.isChecked = false
        ctView.setCheckMarkDrawable(android.R.drawable.checkbox_off_background)
        ctView.setOnClickListener{
            ctView.isChecked = !ctView.isChecked
            ctView.setCheckMarkDrawable(
                if(ctView.isChecked)
                    android.R.drawable.checkbox_on_background
                else
                    android.R.drawable.checkbox_off_background
            )
            val msg = getString(R.string.msg_shown) + " " + getString(
                    if(ctView.isChecked) R.string.checked else R.string.unchecked
                )
            Toast.makeText(CheckedTextViewActivity@this, msg, Toast.LENGTH_LONG).show()
        }
    }
}